<?php
namespace Drupal\redirect_to_content\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RedirectToContentSubscriber implements EventSubscriberInterface {

  public function checkRedirection(GetResponseEvent $event) {
    $request = $event->getRequest();

    if($request->attributes->get('_route') == 'entity.node.canonical' &&
       $request->get('_format') != 'json') {
      $config = \Drupal::config('redirect_to_content.config');
      $url = $config->get('redirect_to') . $request->getPathInfo();
      $response = new TrustedRedirectResponse($url, 301);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([])->addCacheTags(['rendered']));
      $event->setResponse($response);
    }
  }

  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkRedirection');
    return $events;
  }
}
